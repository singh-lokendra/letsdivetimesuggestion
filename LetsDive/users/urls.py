from django.urls import path

from .views import (
    UserDetailList,
    UserDetail,
    UserTimingDetail,
)

urlpatterns = [
    path("", UserDetailList.as_view()),
    path("<int:pk>/", UserDetail.as_view()),
    path(
        "<int:pk>/UserTimingPreferences/",
        UserTimingDetail.as_view(),
    ),
]
