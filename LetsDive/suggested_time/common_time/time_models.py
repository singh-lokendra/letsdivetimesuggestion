from dataclasses import dataclass
from datetime import datetime


@dataclass
class WorkTiming:
    start: datetime
    end: datetime
    uid: int


@dataclass
class BusyTime:
    start: datetime
    end: datetime


@dataclass
class CommonTime(BusyTime):
    pass
