import dataclasses
from collections import Iterator
from datetime import timedelta, datetime
from typing import Optional, Any

from users.models import User
from users.serializers import UserSerializer

from .Errors import NoCommonTimeError
from .calendar import calendar_query
from .convert_utc import utc_work_timing
from .time_models import BusyTime, CommonTime


def list_dataclass_to_dict(results: list[BusyTime]) -> Iterator[dict[str, str]]:
    """
    Iterates over the input list and yields dataclass as dict
    as django can't serialize dataclasses
    """
    for result in results:
        yield dataclasses.asdict(result)


def merge_intervals(time_intervals: list[BusyTime]) -> list[BusyTime]:
    """
    Merge given time intervals by combining overlapping intervals
    """
    ans: list[BusyTime] = []

    for time_interval in time_intervals:
        beg = time_interval.start
        end = time_interval.end
        if not ans or ans[-1].end < beg:
            ans += [BusyTime(beg, end)]
        else:
            ans[-1].end = max(ans[-1].end, end)
    return ans


def merge_busy_schedule(
    users: list, calendar_timing: dict[Any, Any]
) -> tuple[list[BusyTime], CommonTime]:
    """
    Raises NoCommonTimeError if no common work timing are possible for all users

    :param users: list of users
    :param calendar_timing: busy timings of corresponding users
    :return: tuple of sorted & merged busy timings of all users,
    and start and end of common timings
    For eg user1: works [0800hrs - 1700hrs]
    and user2: works [0900hrs - 1800hrs]
    then their common timings are [0900hrs - 1700hrs]
    """

    users = User.objects.filter(pk__in=users)

    serializer = UserSerializer(users, many=True)
    serializer = [utc_work_timing(s) for s in serializer.data]

    common_start_time = max(s.start for s in serializer)
    common_end_time = min(s.end for s in serializer)

    if common_start_time > common_end_time:
        raise NoCommonTimeError

    common_calendar: list[BusyTime] = []
    for u in serializer:
        common_calendar.extend(calendar_query(u.uid, calendar_timing))

    common_calendar.sort(key=lambda x: (x.start, x.end))
    common_time = CommonTime(common_start_time, common_end_time)
    return merge_intervals(common_calendar), common_time


def update_date(common_time: CommonTime, present_day: datetime):
    """
    Mutates common time and updates their date to present day
    Required for timedelta function, because default date of 1900-01-01
    is set by default in common time
    """
    common_time.start = common_time.start.replace(
        year=present_day.year,
        month=present_day.month,
        day=present_day.day,
    )

    common_time.end = common_time.end.replace(
        year=present_day.year,
        month=present_day.month,
        day=present_day.day,
    )


def calculate_fun_time_interval(
    users: list, duration_mins: int, count: int, calendar_timing: dict[Any, Any]
) -> Optional[list[dict[str, str]]]:
    """
    Calculates common fun time

    :param users: list of users
    :param duration_mins: duration of fun time interval in minutes
    :param count: max number of intervals to be returned
    :param calendar_timing: used to calculate Users' busy timing
    :return: list of common fun time if it exists else None
    """
    busy_intervals, common_time = merge_busy_schedule(users, calendar_timing)

    fun_time = []

    if not busy_intervals:
        fun_time_start = common_time.start
        fun_time_end = fun_time_start + timedelta(minutes=duration_mins)
        fun_time = no_busy_interval(
            common_time, duration_mins, fun_time_end, fun_time_start
        )

    else:
        update_date(common_time, busy_intervals[0].start)

        if (
            busy_intervals[0].start < common_time.start
            and busy_intervals[-1].end > common_time.end
        ):
            return []

        fun_time_start = common_time.start
        i = 0
        while True:
            fun_time_end = fun_time_start + timedelta(minutes=duration_mins)

            if fun_time_end <= busy_intervals[i].start:
                fun_time_interval = CommonTime(fun_time_start, fun_time_end)
                fun_time.append(fun_time_interval)
                fun_time_start = fun_time_end

            else:
                fun_time_start = busy_intervals[i].end
                i += 1

            if i == len(busy_intervals) and busy_intervals[-1].end < common_time.end:
                fun_time_start = busy_intervals[-1].end
                fun_time_end = fun_time_start + timedelta(minutes=duration_mins)

                after_work_fun_time = no_busy_interval(
                    common_time, duration_mins, fun_time_end, fun_time_start
                )
                fun_time.extend(after_work_fun_time)

                break

    if fun_time:
        fun_time = fun_time[:count]
        return list(list_dataclass_to_dict(fun_time))


def no_busy_interval(
    common_time: CommonTime,
    duration_mins: int,
    fun_time_end: datetime,
    fun_time_start: datetime,
) -> list[CommonTime]:
    """
    Calculates fun time when all busy schedules are over

    :param common_time: common work timing of all users
    :param duration_mins: duration of fun time interval in minutes
    :param fun_time_end: end of fun time interval
    :param fun_time_start: start of fun time interval
    :return: list of fun time intervals
    """
    fun_time: list[CommonTime] = []
    while fun_time_end <= common_time.end:
        fun_time_interval = CommonTime(fun_time_start, fun_time_end)
        fun_time.append(fun_time_interval)
        fun_time_start = fun_time_end
        fun_time_end = fun_time_start + timedelta(minutes=duration_mins)

    return fun_time
