# Usage
1. To start the app `docker-compose up`
2. To shutdown `docker-compose down`
3. To execute tests cd in to LetsDive directory containing manage.py 
   1. Using unittest `python manage.py test`
   2. Using coverage `coverage run --omit='*env*' manage.py test`
      1. View coverage generated `coverage report --show-missing`
4. docs: `https://docs.docker.com/samples/django/`


# Assumptions
0. Users provide valid timezone name
1. Suggested Timing user working hours is available in db
2. Calendar api returns busy schedule for same day for all users
3. user id is integer and it is the primary key in users database
4. Suggested Time is in UTC, since members can be in different time zones, it's best to provide UTC time
5. Busy timings will take predicted time(no long meetings)
6. Users can switch instantaneously, if meeting is completed at 2:30UTC, then user can switch at 2:30UTC
7. Free timings are continuous, try to avoid free time (can be improved). 
   1. for example if 1 hour free time is available, and for 15 minute slots
   2. Algorithm will suggest 4 time slots i.e. 0m - 15m, 15m - 30m, 30m - 45m, 45m - 60m
   3. Theoretically we can have 46 time slots i.e. 0m - 15m, 1m - 16m, 2m - 17m, ..., 45m - 60m

# Endpoints
1. users end point
   1. `http://localhost:8080/api/v1/users/`
2. work timings
   1. `http://localhost:8080/api/v1/users/1/UserTimingPreferences/`
3. common timings
   1. `http://localhost:8080/api/v1/suggested_time/?users=1,2,3&duration_mins=30&count=3`


# Suggestions/ Improvements
1. `suggested_time` endpoint can have a query param `timezone` and response values will be set to that time zone
2. Right now continuous intervals are provided from possible timings,
   1. it would be better if result have equally spaced timings
   2. for eg, lets say Suggested Timing is available b/w 0800hrs to 1700hrs
   3. and user has queried for 30 mins duration and counts = 2
   4. current solution provides ['0800hrs - 0830hrs', '0830hrs - 0900hrs']
   5. maybe we can provide solution ['0800hrs - 0830hrs', '1630hrs - 1700hrs']