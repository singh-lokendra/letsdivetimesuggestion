FROM python:3.9

ENV PYTHONUNBUFFERED=1

WORKDIR /LetsDiveAssignment

COPY requirements.txt /LetsDiveAssignment/

RUN pip install -r requirements.txt

COPY . /LetsDiveAssignment/
